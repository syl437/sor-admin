app.controller('sorokavideo', ['$scope', '$filter', '$http', 'editableOptions', 'editableThemes', '$rootScope',
  function($scope, $filter, $http, editableOptions, editableThemes,$rootScope){
    editableThemes.bs3.inputClass = 'input-sm';
    editableThemes.bs3.buttonsClass = 'btn-sm';
    editableOptions.theme = 'bs3';
	
	///////Main Contacts Load	
	 $http.get($rootScope.host+'/getVideos.php')
        .success(function(data, status, headers, config)
        {
			//console.log(data.response);
			$scope.videos = data.response;
		//	console.log($rootScope.contacts)
        })
        .error(function(data, status, headers, config)
        {
			
        });
	

	
    $scope.html5 = {
      email: 'email@example.com',
      tel: '123-45-67',
      number: 29,
      range: 10,
      url: 'http://example.com',
      search: 'blabla',
      color: '#6a4415',
      date: null,
      time: '12:30',
      datetime: null,
      month: null,
      week: null
    };

    $scope.user = {
    	name: 'awesome',
    	desc: 'Awesome user \ndescription!',
      status: 2,
      agenda: 1,
      remember: false
    }; 

   
    $scope.showStatus = function() {
      var selected = $filter('filter')($scope.statuses, {value: $scope.user.status});
      return ($scope.user.status && selected.length) ? selected[0].text : 'Not set';
    };
	
	 $scope.isEmployee = function(val) {
      return (val=='1') ? 'כן' : 'לא';
    };

    $scope.showAgenda = function() {
      var selected = $filter('filter')($scope.agenda, {value: $scope.user.agenda});
      return ($scope.user.agenda && selected.length) ? selected[0].text : 'Not set';
    };

   
    $scope.loadGroups = function() {
      return $scope.groups.length ? null : $http.get('api/groups').success(function(data) {
        $scope.groups = data;
      });
    };

    $scope.showGroup = function(user) {
      if(user.group && $scope.groups.length) {
        var selected = $filter('filter')($scope.groups, {id: user.group});
        return selected.length ? selected[0].text : 'Not set';
      } else {
        return user.groupName || 'Not set';
      }
    };

    $scope.showStatus = function(user) {
      var selected = [];
      if(user && user.status) {
        selected = $filter('filter')($scope.statuses, {value: user.status});
      }
      return selected.length ? selected[0].text : 'Not set';
    };

	/*
    $scope.checkName = function(data, id) {
      if (id === 7 && data !== 'awesome') {
        return "Username 2 should be `awesome`";
      }
    };
*/

    $scope.saveUser = function(data,id,index) {
    $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	//console.log(data);
	//alert (data.id);
	if ($scope.videos[index].id == 0)
	{
		$scope.Url = 'add_new_video.php';
	}
	else
	{
		$scope.Url = 'updateVideo.php';
	}
	//alert (id);
		//if (id)
		//{
			update_params = 
			{
				'id' : $scope.videos[index].id,
				'title' : data.title,
				'link' : data.videolink,
				//'subtitle' : data.subtitle,
				//'phone' : data.phone,
				//'link' : $scope.contactlink,
				//'employee' : data.group
				//'image' : $scope.imageUrl
				};
		
        $http.post($rootScope.host+'/'+$scope.Url, update_params)
        .success(function(data, status, headers, config)
        {
			//window.location.href = "#/Contacts/";	

        })
        .error(function(data, status, headers, config)
        {
			//alert("f : " +data.responseText)
            //console.log('error : ' + data);
        });
		
		//}

      //$scope.user not updated yet
      //angular.extend(data, {id: id});
      // return $http.post('api/saveUser', data);
    };

    // remove user
    $scope.removeUser = function(index,id) {
	confirmbox = confirm("האם לאשר מחיקה?");
	if (confirmbox)
	{
	
	$.ajax({
    	type: 'POST',
    	url: $rootScope.host + '/deleteVideo.php',
    	data: { 
        	'id': id
    	},
    	success: function(msg)
		{
		}
	});		
	
      $scope.videos.splice(index, 1);
	  
	}

	  
    };

    // add user
    $scope.addUser = function() {
      $scope.inserted = {
		 id: 0,
        //id: $scope.videos.length+1,
        name: '',
        status: null,
        group: null 
      };
      $scope.videos.push($scope.inserted);
    };

}]);
