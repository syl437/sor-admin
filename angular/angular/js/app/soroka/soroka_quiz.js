app.controller('sorokaquiz', ['$scope', '$filter', '$http', 'editableOptions', 'editableThemes', '$rootScope',
  function($scope, $filter, $http, editableOptions, editableThemes,$rootScope){
    editableThemes.bs3.inputClass = 'input-sm';
    editableThemes.bs3.buttonsClass = 'btn-sm';
    editableOptions.theme = 'bs3';
	
	///////Main Contacts Load	
	 $http.get($rootScope.host+'/getQuestions.php')
        .success(function(data, status, headers, config)
        {
			console.log("Questions");
			console.log(data.response);
			$scope.users = data.response;
		//	console.log($rootScope.contacts)
        })
        .error(function(data, status, headers, config)
        {
			
        });
	

	
    $scope.html5 = {
      email: 'email@example.com',
      tel: '123-45-67',
      number: 29,
      range: 10,
      url: 'http://example.com',
      search: 'blabla',
      color: '#6a4415',
      date: null,
      time: '12:30',
      datetime: null,
      month: null,
      week: null
    };

    $scope.user = {
    	name: 'awesome',
    	desc: 'Awesome user \ndescription!',
      status: 2,
      agenda: 1,
      remember: false
    }; 

   
    $scope.showStatus = function() {
      var selected = $filter('filter')($scope.statuses, {value: $scope.user.status});
      return ($scope.user.status && selected.length) ? selected[0].text : 'Not set';
    };
	
	 $scope.isEmployee = function(val) {
      return (val=='1') ? 'כן' : 'לא';
    };

    $scope.showAgenda = function() {
      var selected = $filter('filter')($scope.agenda, {value: $scope.user.agenda});
      return ($scope.user.agenda && selected.length) ? selected[0].text : 'Not set';
    };

   
    $scope.loadGroups = function() {
      return $scope.groups.length ? null : $http.get('api/groups').success(function(data) {
        $scope.groups = data;
      });
    };

    $scope.showGroup = function(user) {
      if(user.group && $scope.groups.length) {
        var selected = $filter('filter')($scope.groups, {id: user.group});
        return selected.length ? selected[0].text : 'Not set';
      } else {
        return user.groupName || 'Not set';
      }
    };

    $scope.showStatus = function(user) {
      var selected = [];
      if(user && user.status) {
        selected = $filter('filter')($scope.statuses, {value: user.status});
      }
      return selected.length ? selected[0].text : 'Not set';
    };

	/*
    $scope.checkName = function(data, id) {
      if (id === 7 && data !== 'awesome') {
        return "Username 2 should be `awesome`";
      }
    };
*/

    $scope.saveUser = function(data, id,user) 
	{
    $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

	//console.log(data);
	//console.log(id);
	//console.log(user);	
 
	 if (user.id == 0)
	 {
		 $scope.Url = 'add_new_question.php';
		 //alert ('new');
		 //alert (user.title);
	 }
	 else
	 {
		 $scope.Url = 'updateQuestion.php';
		 //alert ('old');
		 //alert (user.id);
		 //alert (user.title);
	 }
	  
	//console.log(data);

				update_params = 
				{
				'id' : user.id,
				'title' : user.title,
				'type' : '1'
				};
		
        $http.post($rootScope.host+'/'+$scope.Url, update_params)
        .success(function(data, status, headers, config)
        {
			//window.location.href = "#/Contacts/";	

        })
        .error(function(data, status, headers, config)
        {
			//alert("f : " +data.responseText)
            //console.log('error : ' + data);
        });
		
		

      //$scope.user not updated yet
      angular.extend(data, {id: id});
      // return $http.post('api/saveUser', data);
    };

    // remove user
    $scope.removeUser = function(index,id) {
	confirmbox = confirm("האם לאשר מחיקה?");
	if (confirmbox)
	{

	$.ajax({
    	type: 'POST',
    	url: $rootScope.host + '/deleteQuestions.php',
    	data: { 
        	'id': id
    	},
    	success: function(msg)
		{
		}
	});		
	
      $scope.users.splice(index, 1);
	  
	}

	  
    };

    // add user
    $scope.addUser = function() 
	{
      $scope.inserted = {
        id: '0',
        title: ''
       // status: null,
       // group: null 
      };
      $scope.users.push($scope.inserted);
    };

}]);
