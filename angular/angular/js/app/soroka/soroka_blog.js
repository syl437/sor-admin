app.controller('BlogCtrl', ['$scope', '$http', '$filter','$rootScope', function($scope, $http, $filter,$rootScope) {
 

	
	//$routeParams.catid
  $http.get($rootScope.host+'/getStories.php').success(function (data) 
  {
	  $scope.groups = data;
	  console.log("rafi")
	  console.log($scope.groups);
	  $scope.items = data[0].articles; 
	  $scope.selectItem($scope.items[0]);
	  //console.log($scope.items);
	  //console.log("Esek")
	  //console.log($scope.items)
		//$scope.catindex = 3;
		//$scope.imgUrl = $rootScope.host;
		//$scope.articlesinfo = data.response;
		//console.log("data")
        //console.log(data)
		
		//$scope.navigatePage = function (Path,Num) 
		//{
			//alert("Path : " + Path)
			//window.location.href = Path+String(Num);	
		//}

  });

  $scope.filter = '';
  $scope.groups1 = [
    {name: 'Coworkers'}, 
    {name: 'Family'}, 
    {name: 'Friends'}, 
    {name: 'Partners'}, 
    {name: 'Group'}
  ];

  $scope.createGroup = function(){
    var group = {name: 'New Group'};
    group.name = $scope.checkItem(group, $scope.groups, 'name');
    $scope.groups.push(group);
  };

  $scope.checkItem = function(obj, arr, key){
    var i=0;
    angular.forEach(arr, function(item) {
      if(item[key].indexOf( obj[key] ) == 0){
        var j = item[key].replace(obj[key], '').trim();
        if(j){
          i = Math.max(i, parseInt(j)+1);
        }else{
          i = 1;
        }
      }
    });
    return obj[key] + (i ? ' '+i : '');
  };

  $scope.deleteGroup = function(item){
	  //console.log (item);
	  //alert (item.index)
	  confirmbox = confirm("האם למחוק קטגורית "+ item.CatName+ '?');
	  if (confirmbox)
	  {
		$.ajax({
			type: 'POST',
			url: $rootScope.host + '/deleteCatagory.php',
			data: { 
				'id': item.index
			},
			success: function(msg)
			{
			}
		});			  

		  $scope.groups.splice($scope.groups.indexOf(item), 1);
	  }
    
  };

  $scope.deleteArticle = function(item)
  {
	  //console.log(item)
	  confirmbox = confirm("האם למחוק כתבה "+ item.ArticleTitle+ '?');
	  
	  if (confirmbox)
	  {
		
		$.ajax({
			type: 'POST',
			url: $rootScope.host + '/deleteArticle.php',
			data: { 
				'id': item.index
			},
			success: function(msg)
			{
			}
		});			  
		
		  $scope.items.splice($scope.items.indexOf(item), 1);
	  }
  }
  $scope.selectGroup = function(item){    
  //console.log (item);
   $scope.CatIndex = item.index;
   $scope.employee = item.employee;
    angular.forEach($scope.groups, function(item) {
      item.selected = false;
    });
    $scope.group = item;
    $scope.group.selected = true;
    $scope.filter = item.articles;
	$scope.items = item.articles;
	$scope.selectItem(item.articles[0]);
  };

  $scope.selectItem = function(item){ 
	console.log("items:");
	console.log(item);
    angular.forEach($scope.items, function(item) {
      item.selected = false;
      item.editing = false;
    });
    $scope.item = item;
	$scope.avatar = $rootScope.host +"/"+item.ArticleImage;
	console.log("Item")
	console.log(item)
    $scope.item.selected = true;
  };

  $scope.deleteItem = function(item){
	  
    $scope.items.splice($scope.items.indexOf(item), 1);
    $scope.item = $filter('orderBy')($scope.items, 'first')[0];
    if($scope.item) $scope.item.selected = true;
  };

  $scope.createItem = function(){
    var item = {
      group: 'Friends',
      avatar:'img/a0.jpg'
    };
    $scope.items.push(item);
    $scope.selectItem(item);
    $scope.item.editing = true;
  };

  $scope.editItem = function(item){
    if(item && item.selected){
      item.editing = true;
    }
  };

  	$scope.onFileSelect = function (file) 
	{
		var fd = new FormData();
        fd.append('fileUPP', file.files[0]);
		//console.log(fd)
		//console.log(file.files[0])
				
		$http.post($rootScope.host+'/UploadImg.php', fd, { transformRequest: angular.identity,"headers" : { "Content-Type" : undefined }}).success(function(data) 
		{
			if (data.response.imagepath) {
				
				$scope.imageUrl = data.response.imagepath;
				$scope.avatar = $rootScope.host+data.response.imagepath;
				//alert ($scope.imageUrl)
			}
         	//alert("p: "+data)  
        }).error(function(data) 
		{
			//alert(data.message); 	
		});
	};
	
	
  $scope.doneEditing = function(item){
    item.editing = false;
	if ($scope.imageUrl)
	{
		$scope.image = $scope.imageUrl;
	}
	else 
	{
		$scope.image = item.ArticleImage;
	}
	console.log(item);
	//alert (item.CatIndex);
	//alert (item.index)
	//alert (item.ArticleTitle)
	//alert (item.ArticleDesc)
	//alert (item.ArticleImage);
	//alert ($scope.item.ArticleDesc);
	//alert (item.test);
	
			if (item.index) {
			$scope.LinkUrl = $rootScope.host+'/updateArticle.php'
			}
			else {
				$scope.LinkUrl = $rootScope.host+'/add_new_article.php'
			}
		//alert ( item.articledesc);
		  $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';


		article_params = 
		{	
			'catindex' : $scope.CatIndex,
		    'id' : item.index,
			'title' : item.ArticleTitle,
			'desc' : item.ArticleDesc,
			'image' : $scope.image,
			'employee' : $scope.employee
			};
			
		//	console.log(data);

		
        $http.post($scope.LinkUrl, article_params)
        .success(function(data, status, headers, config)
        {

        })
        .error(function(data, status, headers, config)
        {
			//alert("f : " +data.responseText)
            //console.log('error : ' + data);
        });
		
	
   
	
	
  };

}]);