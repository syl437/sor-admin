app.controller('sorokanewemployee', ['$scope', '$http', '$filter','$rootScope', function($scope, $http, $filter,$rootScope) {
/*
  $http.get('js/app/contact/contacts.json').then(function (resp) {
    $scope.items = resp.data.items;
    $scope.item = $filter('orderBy')($scope.items, 'first')[0];
    $scope.item.selected = true;
	
	console.log($scope.items)
  });
*/

	///////Main Trading Load	
	 $http.get($rootScope.host+'/getEmlopyeeMonth.php')
        .success(function(data, status, headers, config)
        {
			//console.log(data.response);
			$scope.item = data.response;
			$scope.avatar = $rootScope.host+data.response.image;
			$scope.item.editTitle = true;
			//$scope.item.editing = true;
			//alert ($scope.avatar)
			//console.log($scope.items)
			//$scope.item = $filter('orderBy')($scope.items, 'title')[0];
			//$scope.item.selected = true;
			
			//$scope.selectItem($scope.items[0])
		//	console.log($rootScope.contacts)
        })
        .error(function(data, status, headers, config)
        {
			
        });

	$scope.onFileSelect = function (file) 
	{
		var fd = new FormData();
        fd.append('fileUPP', file.files[0]);
		console.log(fd)
		console.log(file.files[0])
				
		$http.post($rootScope.host+'/UploadImg.php', fd, { transformRequest: angular.identity,"headers" : { "Content-Type" : undefined }}).success(function(data) 
		{
			if (data.response.imagepath) {
				
				$scope.imageUrl = data.response.imagepath;
				$scope.avatar = $rootScope.host+data.response.imagepath;
				//alert ($scope.imageUrl)
			}
         	//alert("p: "+data)  
        }).error(function(data) 
		{
			//alert(data.message); 	
		});
	};		

  $scope.filter = '';
  $scope.groups = [
    {name: 'Coworkers'}, 
    {name: 'Family'}, 
    {name: 'Friends'}, 
    {name: 'Partners'}, 
    {name: 'Group'}
  ];

  $scope.createGroup = function(){
    var group = {name: 'New Group'};
    group.name = $scope.checkItem(group, $scope.groups, 'name');
    $scope.groups.push(group);
  };

  $scope.checkItem = function(obj, arr, key){
    var i=0;
    angular.forEach(arr, function(item) {
      if(item[key].indexOf( obj[key] ) == 0){
        var j = item[key].replace(obj[key], '').trim();
        if(j){
          i = Math.max(i, parseInt(j)+1);
        }else{
          i = 1;
        }
      }
    });
    return obj[key] + (i ? ' '+i : '');
  };

  $scope.deleteGroup = function(item){
	  confirmbox = confirm("האם לאשר מחיקה?");
	  if (confirmbox)
	  {
		$.ajax({
			type: 'POST',
			url: $rootScope.host + '/deleteMarket2.php',
			data: { 
				'id': item.id
			},
			success: function(msg)
			{
				
			}
		});	
		$scope.items.splice($scope.items.indexOf(item), 1);
	  }
    
  };

  $scope.selectGroup = function(item){    
    angular.forEach($scope.groups, function(item) {
      item.selected = false;
    });
	console.log("Item")
	console.log(item)
    $scope.item = item;
	//$scope.avatar = $rootScope.host +"/"+$scope.item.image
    $scope.group.selected = true;
    $scope.filter = item.name;
  };

  $scope.selectItem = function(item){   
    angular.forEach($scope.items, function(item) {
      item.selected = false;
      item.editing = false;
	  item.editTitle = true;
    });
	console.log(item)
    $scope.item = item;
	//$scope.avatar = $rootScope.host +"/"+$scope.item.image
    $scope.item.selected = true;
  };

  $scope.deleteItem = function(item){
    $scope.items.splice($scope.items.indexOf(item), 1);
    $scope.item = $filter('orderBy')($scope.items, 'first')[0];
    if($scope.item) $scope.item.selected = true;
  };

  $scope.createItem = function(){
    var item = {
      group: 'Friends',
      avatar:'img/a0.jpg'
    };
    $scope.items.push(item);
    $scope.selectItem(item);
    $scope.item.editing = true;
  };

  $scope.editItem = function(item){
    if(item && item.selected){
      item.editing = true;
	  item.editTitle = true;
    }
  };

  $scope.doneEditing = function(item)
  {
  	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	
	
	$scope.LinkUrl = $rootScope.host+'/updateEmployeeMonth.php';
	
	if ($scope.imageUrl)
	{
		$scope.newimage = $scope.imageUrl;
	}
	else
	{
		$scope.newimage = item.image;
	}
	
	
		update_params = 
		{
		    'id' : "1",
			'title' : item.title,
			'desc' : item.desc,
		//	'link' : $scope.videolink,
			'image' : $scope.newimage
			};

        $http.post($scope.LinkUrl, update_params)
        .success(function(data, status, headers, config)
        {
			alert ("נשמר בהצלחה");
			//window.location.href = "#/Market/";	

        })
        .error(function(data, status, headers, config)
        {
			//alert("f : " +data.responseText)
            //console.log('error : ' + data);
        });
			
	
	//$scope.item.editing = true;
	
	//$scope.item.editTitle = false;
	
	  //alert (item.title);
	  //alert (item.image);
    //item.editing = false;
	//item.editTitle = false;
  };

}]);